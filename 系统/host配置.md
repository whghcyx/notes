---
layout: post  
title: Host配置  
date: 2019-9-26 10:42:0  
group:   
tags:   
---
---
# Host配置 #   

## Windows ##
host路径：

```
C:\Windows\System32\drivers\etc\hosts
```

## Linux ##
host路径：
```
/etc/hosts
```

## Mac ##
host路径：

## 要添加的内容 ##
```
192.30.253.112 github.com
151.101.185.194 github.global.ssl.fastly.net
151.101.189.194 github.global.ssl.fastly.net
219.76.4.4 github-cloud.s3.amazonaws.com
52.216.179.107
13.250.177.223 githoub.com
52.74.223.119 githoub.com
```


---
**签名：Smile every day**  
**名字：沐阳宏**  
**邮箱：whghcyx@outlook.com**  
---
