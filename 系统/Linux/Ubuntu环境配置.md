---
layout: post  
title: Ubuntu环境配置  
date: 2019-9-30  
group:  
tags:  
---
---
# Ubuntu环境配置 #
- 查看版本信息  
lsb_release -a  
- 查看系统信息  
uname -a
- 查看内核、gcc  
cat /proc/version
## 更改源 ##


阿里源
```
mv sources.list sources.list.bak
echo "# software repository" > sources.list
echo "deb http://mirrors.aliyun.com/ubuntu/ bionic main restricted universe multiverse                " >> sources.list
echo "deb http://mirrors.aliyun.com/ubuntu/ bionic-security main restricted universe multiverse       " >> sources.list
echo "deb http://mirrors.aliyun.com/ubuntu/ bionic-updates main restricted universe multiverse        " >> sources.list
echo "deb http://mirrors.aliyun.com/ubuntu/ bionic-proposed main restricted universe multiverse       " >> sources.list
echo "deb http://mirrors.aliyun.com/ubuntu/ bionic-backports main restricted universe multiverse      " >> sources.list
echo "deb-src http://mirrors.aliyun.com/ubuntu/ bionic main restricted universe multiverse            " >> sources.list
echo "deb-src http://mirrors.aliyun.com/ubuntu/ bionic-security main restricted universe multiverse   " >> sources.list
echo "deb-src http://mirrors.aliyun.com/ubuntu/ bionic-updates main restricted universe multiverse    " >> sources.list
echo "deb-src http://mirrors.aliyun.com/ubuntu/ bionic-proposed main restricted universe multiverse   " >> sources.list
echo "deb-src http://mirrors.aliyun.com/ubuntu/ bionic-backports main restricted universe multiverse  " >> sources.list
```
中科大源
```
mv sources.list sources.list.bak
echo "# software repository" > sources.list
echo "deb https://mirrors.ustc.edu.cn/ubuntu/ bionic main restricted universe multiverse               " >> sources.list
echo "deb-src https://mirrors.ustc.edu.cn/ubuntu/ bionic main restricted universe multiverse           " >> sources.list
echo "deb https://mirrors.ustc.edu.cn/ubuntu/ bionic-updates main restricted universe multiverse       " >> sources.list
echo "deb-src https://mirrors.ustc.edu.cn/ubuntu/ bionic-updates main restricted universe multiverse   " >> sources.list
echo "deb https://mirrors.ustc.edu.cn/ubuntu/ bionic-backports main restricted universe multiverse     " >> sources.list
echo "deb-src https://mirrors.ustc.edu.cn/ubuntu/ bionic-backports main restricted universe multiverse " >> sources.list
echo "deb https://mirrors.ustc.edu.cn/ubuntu/ bionic-security main restricted universe multiverse      " >> sources.list
echo "deb-src https://mirrors.ustc.edu.cn/ubuntu/ bionic-security main restricted universe multiverse  " >> sources.list
echo "deb https://mirrors.ustc.edu.cn/ubuntu/ bionic-proposed main restricted universe multiverse      " >> sources.list
echo "deb-src https://mirrors.ustc.edu.cn/ubuntu/ bionic-proposed main restricted universe multiverse  " >> sources.list
apt-get update
apt-get upgrade
```
163源
```
mv sources.list sources.list.bak
echo "# software repository" > sources.list
echo "deb http://mirrors.163.com/ubuntu/ bionic main restricted universe multiverse                " >> sources.list
echo "deb http://mirrors.163.com/ubuntu/ bionic-security main restricted universe multiverse       " >> sources.list
echo "deb http://mirrors.163.com/ubuntu/ bionic-updates main restricted universe multiverse        " >> sources.list
echo "deb http://mirrors.163.com/ubuntu/ bionic-proposed main restricted universe multiverse       " >> sources.list
echo "deb http://mirrors.163.com/ubuntu/ bionic-backports main restricted universe multiverse      " >> sources.list
echo "deb-src http://mirrors.163.com/ubuntu/ bionic main restricted universe multiverse            " >> sources.list
echo "deb-src http://mirrors.163.com/ubuntu/ bionic-security main restricted universe multiverse   " >> sources.list
echo "deb-src http://mirrors.163.com/ubuntu/ bionic-updates main restricted universe multiverse    " >> sources.list
echo "deb-src http://mirrors.163.com/ubuntu/ bionic-proposed main restricted universe multiverse   " >> sources.list
echo "deb-src http://mirrors.163.com/ubuntu/ bionic-backports main restricted universe multiverse  " >> sources.list
```
清华源
```
mv sources.list sources.list.bak
echo "# software repository" > sources.list
echo "deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ bionic main restricted universe multiverse               " >> sources.list
echo "deb-src https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ bionic main restricted universe multiverse           " >> sources.list
echo "deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ bionic-updates main restricted universe multiverse       " >> sources.list
echo "deb-src https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ bionic-updates main restricted universe multiverse   " >> sources.list
echo "deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ bionic-backports main restricted universe multiverse     " >> sources.list
echo "deb-src https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ bionic-backports main restricted universe multiverse " >> sources.list
echo "deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ bionic-security main restricted universe multiverse      " >> sources.list
echo "deb-src https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ bionic-security main restricted universe multiverse  " >> sources.list
echo "deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ bionic-proposed main restricted universe multiverse      " >> sources.list
echo "deb-src https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ bionic-proposed main restricted universe multiverse  " >> sources.list
```

### 安装软件 ###
apt-get install snap

### 修改hosts ###
/etc/hosts
192.30.253.112  github.com

---
<div style="text-align: right">
<p><strong>签名：Smile every day</strong></p>
<p><strong>名称：沐阳宏</strong></p>
<p><strong>邮箱：</strong><a href="whghcyx@outlook.com">whghcyx@outlook.com</a></p>
</div>
---
---
