
>**Title**: 模型下载  
>**Date**: 2019-9-30 15:19:54  
>**Tags**:   

---
# 模型下载  #   
### Alexnet: ###
```
model_urls = {
    'alexnet': 'https://download.pytorch.org/models/alexnet-owt-4df8aa71.pth',
}
```
### cascade: ###
```
model_urls = {
    'cascade': 'https://www.dropbox.com/home/DetNet?preview=detnet59.pth
}
```
### Densenet: ###
```
model_urls = {
    'densenet121': 'https://download.pytorch.org/models/densenet121-a639ec97.pth',
    'densenet169': 'https://download.pytorch.org/models/densenet169-b2777c0a.pth',
    'densenet201': 'https://download.pytorch.org/models/densenet201-c1103571.pth',
    'densenet161': 'https://download.pytorch.org/models/densenet161-8d451a50.pth',
}
```
### googlenet: ###
model_urls = {
    # GoogLeNet ported from TensorFlow
    'googlenet': 'https://download.pytorch.org/models/googlenet-1378be20.pth',
}

### inception: ###
```
model_urls = {
    # Inception v3 ported from TensorFlow
    'inception_v3_google': 'https://download.pytorch.org/models/inception_v3_google-1a9a5a14.pth',
}
```
### mobilenet: ###
model_urls = {
    'mobilenet_v2': 'https://download.pytorch.org/models/mobilenet_v2-b0353104.pth',
}


## Resnet: ##

```
model_urls = {  
    'resnet18': 'https://download.pytorch.org/models/resnet18-5c106cde.pth',  
    'resnet34': 'https://download.pytorch.org/models/resnet34-333f7ec4.pth', 
    'resnet50': 'https://download.pytorch.org/models/resnet50-19c8e357.pth',  
    'resnet101': 'https://download.pytorch.org/models/resnet101-5d3b4d8f.pth',  
    'resnet152': 'https://download.pytorch.org/models/resnet152-b121ed2d.pth',
}
```
## shufflenet: ##
model_urls = {
    'shufflenetv2_x0.5': 'https://download.pytorch.org/models/shufflenetv2_x0.5-f707e7126e.pth',
    'shufflenetv2_x1.0': 'https://download.pytorch.org/models/shufflenetv2_x1-5666bf0f80.pth',
    'shufflenetv2_x1.5': None,
    'shufflenetv2_x2.0': None,
}
## squeezenet: ##
model_urls = {
    'squeezenet1_0': 'https://download.pytorch.org/models/squeezenet1_0-a815701f.pth',
    'squeezenet1_1': 'https://download.pytorch.org/models/squeezenet1_1-f364aa15.pth',
}
### vggnet: ###

```
model_urls = {
    'vgg11': 'https://download.pytorch.org/models/vgg11-bbd30ac9.pth',
    'vgg13': 'https://download.pytorch.org/models/vgg13-c768596a.pth',
    'vgg16': 'https://download.pytorch.org/models/vgg16-397923af.pth',
    'vgg19': 'https://download.pytorch.org/models/vgg19-dcbb9e9d.pth',
    'vgg11_bn': 'https://download.pytorch.org/models/vgg11_bn-6002323d.pth',
    'vgg13_bn': 'https://download.pytorch.org/models/vgg13_bn-abd245e5.pth',
    'vgg16_bn': 'https://download.pytorch.org/models/vgg16_bn-6c64b313.pth',
    'vgg16':    'https://s3.amazonaws.com/amdegroot-models/vgg16_reducedfc.pth',
    'vgg19_bn': 'https://download.pytorch.org/models/vgg19_bn-c79401a0.pth',
}
```




---
**签名：Smile every day**    
**名字：沐阳宏**   
**邮箱：whghcyx@outlook.com**  
---
