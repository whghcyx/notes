
>**Title**: SSD模型训练指南  
>**Date**: 2019-10-23   
>**Tags**:   

---
# SSD模型训练指南  #  
## 1、Git代码 ##
[https://github.com/amdegroot/ssd.pytorch](https://github.com/amdegroot/ssd.pytorch)
## 2、环境安装 ##
```

pip install visdom

```
## 3、模型下载 ##
[https://s3.amazonaws.com/amdegroot-models/vgg16_reducedfc.pth](https://s3.amazonaws.com/amdegroot-models/vgg16_reducedfc.pth)
## 4、数据集制作 ##




---
**签名：Smile every day**    
**名字：沐阳宏**   
**邮箱：whghcyx@outlook.com**  
---
