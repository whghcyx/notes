---
layout: post  
title: Torch常用层  
date: 2019-10-30  
group:   
tags:   
---
---
# Torch常用层 #

|层名字|简称|功能|
|:-|:-|:-|
|**卷积层** | | | 
|torch.nn.Conv1d|一维卷积 | | |
|torch.nn.Conv2d |二维卷积 | |  
|torch.nn.Conv3d |三维卷积 | |  
|**反卷积层** | | | 
|torch.nn.ConvTranspose1d |一维反卷积 | |  
|torch.nn.ConvTranspose2d |二维反卷积 | |  
|torch.nn.ConvTranspose3d |三维反卷积 | |  
|**激活层** | | |    
|nn.CELU               | | |
|nn.ELU                | | |
|nn.GLU                | | |
|nn.Hardshrink         | | |
|nn.Hardtanh           | | |
|nn.ReLU6              | | |
|nn.LeakyReLU          | | |
|nn.LogSigmoid         | | |
|nn.LogSoftmax         | | |
|nn.MultiheadAttention | | |
|nn.PReLU              | | |
|nn.RReLU              | | |
|nn.ReLU               | | |
|nn.SELU               | | |
|nn.Sigmoid            | | |
|nn.Softmax            | | |
|nn.Softmax2d          | | |
|nn.Softmin            | | |
|nn.Softplus           | | |
|nn.Softshrink         | | |
|nn.Softsign           | | |
|nn.Tanh               | | |
|nn.Tanhshrink         | | |
|nn.Threshold          | | |
|**批量归一化层** | | | 
|nn.BatchNorm1d/2d/3d     | | |
|nn.GroupNorm             | | |
|nn.InstanceNorm1d/2d/3d  | | |
|nn.LayerNorm             | | |
|LocalResponseNorm        |(LRN) | |
|**惩罚层** | | | 
|nn.Dropout      |一维惩罚层 | |
|nn.Dropout2d    | | |
|nn.Dropout3d    | | |
|nn.AlphaDropout | | |
|**池化层** | | | 
|nn.MaxPool1d/2d/3d         |最大池化 | | 
|nn.MaxUnpool1d/2d/3d       | | | 
|nn.AvgPool1d/2d/3d         | | | 
|nn.FractionMaxPool2d       | | | 
|nn.AdaptiveMaxPool1d/2d/3d | | | 
|nn.AdaptiveAvgPool1d/2d/3d | | | 
| | | | 
| | | | 
|**线性层** | | | 
|Linear()   |           | |
|Identity()   |           | |
|Bilinear() |双线性变换 | |
| | | | 
| | | | 
| | | | 
| | | | 






---
<div style="text-align: right">
<p><strong>签名：Smile every day</strong></p>
<p><strong>名称：沐阳宏</strong></p>
<p><strong>邮箱：</strong><a href="whghcyx@outlook.com">whghcyx@outlook.com</a></p>
</div>
---
---
