
>**Title**: Linux环境安装  
>**Date**: 2019-11-4  
>**Tags**:   

---
# Linux环境安装 #  

## 用户 ##
查看用户  
> cat /etc/passwd

添加用户

更改用户


## 组 ##
查看组  
> cat /etc/group

添加组  

更改组  

chgrp -R anaconda /etc/anaconda


---
**签名：Smile every day**    
**名字：沐阳宏**   
**邮箱：whghcyx@outlook.com**  
---
