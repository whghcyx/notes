
>**Title**: CenterNet模型学习  
>**Date**: 2019-11-15  
>**Tags**:   

---
# CenterNet模型学习  #  
## 论文地址 ##
https://arxiv.org/abs/1904.08189


## Git代码 ##
1、[https://github.com/Duankaiwen/CenterNet](https://github.com/Duankaiwen/CenterNet)  原版代码（**环境安装太难**）
2、[https://github.com/UmarSpa/CenterNet](https://github.com/UmarSpa/CenterNet)  对代码做了修补（**使用这份代码进行环境安装，与代码使用**）

## 环境安装 ##
- 硬件配置

|硬件|配置|说明|
|:-|:-|:-|
|CUDA| 10.2|10以上就可以。最重要，我只试过这个版本|
|anconda |4.6.11||
|NVIDIA RTX| 2080| 
|Driver Version| 430.14|
|系统|ubuntu16.04.1|Linux系统会方便一些|

- 安装

```
## 环境与PyTorch包
conda env create -f packagelist.yml
conda activate CenterNet-PT10
conda install pytorch==1.0.0 torchvision==0.2.1 cuda100 -c pytorch

## 编译Corner Pooling Layers
cd models/py_utils/_cpools/
python setup.py install --user

## 编译NMS
cd data/coco/PythonAPI
make
```

## 数据集下载 ##
[http://images.cocodataset.org/zips/train2014.zip](http://images.cocodataset.org/zips/train2014.zip)
[http://images.cocodataset.org/zips/val2014.zip](http://images.cocodataset.org/zips/val2014.zip)
[http://images.cocodataset.org/zips/test2017.zip](http://images.cocodataset.org/zips/test2017.zip)
[http://images.cocodataset.org/annotations/annotations_trainval2014.zip](http://images.cocodataset.org/annotations/annotations_trainval2014.zip)
[http://images.cocodataset.org/annotations/image_info_test2017.zip](http://images.cocodataset.org/annotations/image_info_test2017.zip)
也可以进这个网站自己下载[http://cocodataset.org/#download](http://cocodataset.org/#download)

---
**签名：Smile every day**    
**名字：沐阳宏**   
**邮箱：whghcyx@outlook.com**  
---
