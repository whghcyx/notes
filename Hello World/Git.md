---
layout: post  
title: Git代码说明  
date: 2019-9-30 16:37:27  
group:   
tags: 标计  
---
---
# Git代码说明 #

- 地址说明  
```
https://github.com/eriklindernoren/PyTorch-YOLOv3  
https://github.com/eriklindernoren/PyTorch-YOLOv3.git  
git@github.com:eriklindernoren/PyTorch-YOLOv3.git  
```
|Git项目地址|说明|
|:-|:-|
|git@github.com:dusty-nv/jetson-inference.git| jetson开发板的学习项目|
|git@github.com:eriklindernoren/PyTorch-YOLOv3.git|Pytorch-Yolov3模型训练|
|https://github.com/opencv/open_model_zoo|包含了大量的计算机视觉预训练模型，并提供了下载方法|
|https://github.com/snowkylin/tensorflow-handbook|tensorflow2.0学习教程|



---
<div style="text-align: right">
<p><strong>签名：Smile every day</strong></p>
<p><strong>名称：沐阳宏</strong></p>
<p><strong>邮箱：</strong><a href="whghcyx@outlook.com">whghcyx@outlook.com</a></p>
</div>
---
---
