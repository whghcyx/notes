
>**Title**: Windows-Python环境安装  
>**Date**: 2019-10-8  
>**Tags**:   

---
# Windows-Python环境安装  #  
## 软件下载 ## 

[python-3.7.5-amd64.exe](https://www.python.org/ftp/python/3.7.5/python-3.7.5-amd64.exe)
## 软件安装 ##


## pip环境安装 ##
1. 更换源的位置
在用户目录内，建立一个目录 pip
在其内建立一个文件 pip.ini
其内内容为：
>[global]
index-url = https://pypi.tuna.tsinghua.edu.cn/simple
[install]
trusted-host=mirrors.aliyun.com
2. pip环境更新
>python -m pip install --upgrade pip
3. 安装tensorflow
>pip install tensorflow-gpu

检测：
```
import tensorflow as tf

v1 = tf.Variable(tf.constant(1.0, shape=[1]), name="v1")
v2 = tf.Variable(tf.constant(2.0, shape=[1]), name="v2")
result = v1 + v2

with tf.Session() as sess:
    print(sess.run(result))  # [ 3.]
```

4. 安装keras
>pip install keras
5. 安装PyTorch
>pip install torch==1.2.0 torchvision==0.4.0 -f https://download.pytorch.org/whl/torch_stable.html

检测：
```
from __future__ import print_function
import torch
x = torch.rand(5, 3)
print(x)
print(torch.cuda.is_available())
```
6. 安装jupyter-tensorboard
>pip install jupyter-tensorboard
7. 安装matplotlib


---
**签名：Smile every day**    
**名字：沐阳宏**   
**邮箱：whghcyx@outlook.com**  
---
