
>**Title**: c转换python  
>**Date**: 2019-10-8  
>**Tags**:   

---
# C转换Python  #  
目录内文件：
>calc.cpp
>setup.py

- **calc.cpp**
```
// 文件名 calc.c
#include

int add(int x, int y){ // C 函数
 return x + y;
}

static PyObject *calc_add(PyObject *self, PyObject *args){

 int x, y;
 // Python传入参数
 // "ii" 表示传入参数为2个int型参数，将其解析到x, y变量中
 if(!PyArg_ParseTuple(args, "ii", &x, &y))
 return NULL;
 return PyLong_FromLong(add(x, y));
}

// 模块的方法列表
static PyMethodDef CalcMethods[] = {
 {"add", calc_add, METH_VARARGS, "函数描述"},
 {NULL, NULL, 0, NULL}
};

// 模块
static struct PyModuleDef calcmodule = {
 PyModuleDef_HEAD_INIT,
 "calc", // 模块名
    NULL, // 模块文档
 -1, /* size of per-interpreter state of the module,
                or -1 if the module keeps state in global variables. */
 CalcMethods
};

// 初始化
PyMODINIT_FUNC PyInit_calc(void)
{
 return PyModule_Create(&calcmodule);
}


```

- **setup.py**
```
from setuptools import setup, Extension, find_packages

module1 = Extension('wcdemo',
                    sources=['wcdemo.c'])

setup(
    name='wcdemo_model',                        # 模块名
    version='1.0',                              # 版本
    keywords=["fractal", "分形"],               # 关键字
    description='Hello ?',                      # 描述
    license="MIT",                              # 协议
    author="whg",                               # 作者
    author_email="whghcyx@outlook.com",         # 邮箱
    ext_modules=[module1],                      # 包含源数据
    packages=find_packages(),                   # 找到包下的所有包
    install_requires=["pygame>=1.9.3"],         # 依赖
    platforms="any",                            # 运行平台
    url="https://github.com/WHG555/wcdemo",     # 项目地址
    zip_safe=False,
    packages_data={                             # 数据文件
        "": ["*.dll", "*.pyd", "*.so"]
    }
)
```



---
**签名：Smile every day**    
**名字：沐阳宏**   
**邮箱：whghcyx@outlook.com**  
---
