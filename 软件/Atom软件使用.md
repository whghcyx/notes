---
layout: post  
title: Atom软件使用  
date: 2019-9-29  
group:  
tags:  
---
---
# Atom软件使用 #

|核心插件名称|说明|
|:-|:-|
|tree-view|浏览并打开当前项目中的文件|
|autosave|自动保存文件|
|link|打开文件中的链接|
|tabs|为每个打开的编辑器显示一个可选择的选项卡|
|settings-vies|编辑配置设置、安装包和更改主题|
|**markdown插件**|**说明**|
|markdown-preview-enhanced|增强预览|
|language-markdown|代码增强|
|markdown-table-editor|表格插件 ？？？|
|**外部插件名称**|**功能**|
|terminal-panel|用于执行命令并显示输出|
|docblockr|可以帮助我们方便快速地写注释|
|file-icons|让文件前面有彩色图片，使文件类型看得更加清除舒服|
|simplified-chinese-menu|将软件改为中文|
|gitee 1.7.2|Gitee上传下载代码支持|
|highlight-selected|高亮相同的单词|
|activate-power-mode|一个特效插件，当连击数达到一定值后每敲一次键盘都会有颗粒特效和震动效果|
|minimap|预览全部代码的一个插件，同时能方便的移动到指定的文件位置|
|sync-setting|同步插件的功能|


![Uploading QQ截图20191013193542.png… (abd7kpv2t)]()

![Uploading QQ截图20191013193542.png… (xhvsehwkh)]()

![Uploading QQ截图20191013193542.png… (iwenosz6z)]()



# 更改插件的位置 #  
Atom.exe的位置  E:\Soft\atom-x64-windows\Atom x64\atom.exe  
将用户目录内的 **.atom**目录拷贝到E:\Soft\atom-x64-windows下  

**目录结构：**
>E:\Soft\atom-x64-windows
>>Atom x64  
>>.atom  

## 关闭自动删除空格 ##
插件 whitespace  
选项 Remove Trailing Whitespace


---
<div style="text-align: right">
<p><strong>签名：Smile every day</strong></p>
<p><strong>名称：沐阳宏</strong></p>
<p><strong>邮箱：</strong><a href="whghcyx@outlook.com">whghcyx@outlook.com</a></p>
</div>
---
---
