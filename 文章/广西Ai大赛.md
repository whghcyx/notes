>**Title**: 文章  
>**Date**: 2019-9-30 15:19:54  
>**Tags**:   

---
# 文章  #   

## 模型参数配置说明 ##
|参数|说明|
|:-|:-|
|num_epochs |训练需要迭代的次数，默认为“30”。
|batch_size |训练的每次更新包含的样本数量大小， 默认为“128”。
|lr | 学习率，默认为“0.1”。
|lr_step |学习率减小的epoch，默认为“16,24,27”。也就是说学习率会在第16个epoch结束时减小为原来的0.1倍，为0.01，第24、27epoch同理。
|num_layers |resnet模型的卷积层数，可供选择的有“18”、“34”、“50”，默认为“34”
|disp_batches |每隔多少步输出一次，默认为“20”。
| **指标值** | **说明**
|Validation-accuracy | 预测的置信度最高的类别是正确的比例，即上述推理测试时输出predicted_label正确占总预测结果的比例，结果越高，模型越好。在深度学习领域一般称为top-1
| Validation-cross-entropy | 交叉熵，用来评判对每一类预测准确率的损失值，越小越好
| Validation-top_k_accuracy_5| 预测的置信度最高的前5类中如果有正确的那一类，就认为这次预测是正确的。在深度学习领域一般称为top-5，同样，结果越高，模型越好。


### V0004 ###
|参数|说明|
|:-|:-|
num_epochs                  | 10
batch_size                  | 128
lr                          | 0.1
lr_step                     | 16,24,27
num_layers                  | 34
disp_batches                | 20
Validation-accuracy         | 0.788504
Validation-cross-entropy    | 0.782395
Validation-top_k_accuracy_5 | 0.930804

### V0005 ###
|参数|说明|
|:-|:-|
num_epochs                  | 10
batch_size                  | 32
lr                          | 0.1
lr_step                     | 10,20,27
num_layers                  | 50
disp_batches                | 20
Validation-accuracy         | 0.597588
Validation-cross-entropy    | 2.352241
Validation-top_k_accuracy_5 | 0.807566

### V0007 ###
|参数|说明|
|:-|:-|
num_epochs                  | 10
batch_size                  | 128
lr                          | 0.1
lr_step                     | 12,20,27
num_layers                  | 50
disp_batches                | 20
Validation-accuracy         | 0.690848
Validation-cross-entropy    | 1.267378
Validation-top_k_accuracy_5 | 0.871094

### V0009 ###
|参数|说明|
|:-|:-|
num_epochs                  | 30 
batch_size                  | 128
lr                          | 0.1
lr_step                     | 16,24,27
num_layers                  | 34
disp_batches                | 20
Validation-accuracy         | 0.780134
Validation-cross-entropy    | 0.780077
Validation-top_k_accuracy_5 | 0.938616

### V0008 ###
|参数|说明|
|:-|:-|
num_epochs                  | 
batch_size                  | 
lr                          | 
lr_step                     | 
num_layers                  | 
disp_batches                | 
Validation-accuracy         | 
Validation-cross-entropy    | 
Validation-top_k_accuracy_5 | 

### V0008 ###
|参数|说明|
|:-|:-|
num_epochs                  | 
batch_size                  | 
lr                          | 
lr_step                     | 
num_layers                  | 
disp_batches                | 
Validation-accuracy         | 
Validation-cross-entropy    | 
Validation-top_k_accuracy_5 | 

### V0008 ###
|参数|说明|
|:-|:-|
num_epochs                  | 
batch_size                  | 
lr                          | 
lr_step                     | 
num_layers                  | 
disp_batches                | 
Validation-accuracy         | 
Validation-cross-entropy    | 
Validation-top_k_accuracy_5 | 

### V0008 ###
|参数|说明|
|:-|:-|
num_epochs                  | 
batch_size                  | 
lr                          | 
lr_step                     | 
num_layers                  | 
disp_batches                | 
Validation-accuracy         | 
Validation-cross-entropy    | 
Validation-top_k_accuracy_5 | 
---
**签名：Smile every day**    
**名字：沐阳宏**   
**邮箱：whghcyx@outlook.com**  
---
