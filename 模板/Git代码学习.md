
>**Title**: CenterNet模型学习  
>**Date**: 2019-11-15  
>**Tags**:   

---
# CenterNet模型学习  #  
## 论文地址 ##
https://arxiv.org/abs/1708.02002
## Git代码 ##
[https://github.com/fizyr/keras-retinanet](https://github.com/fizyr/keras-retinanet)
## 环境安装 ##
- 硬件配置


- 安装
```
conda create -n retinanet python=3.7
conda activate retinanet
conda install tensorflow-gpu==1.13.1
conda install keras-gpu
pip install numpy
pip install .
python setup.py build_ext --inplace
```
## 预训练模型下载 ##
https://github.com/fizyr/keras-retinanet/releases/download/0.5.1/resnet50_coco_best_v2.1.0.h5
https://github.com/fizyr/keras-retinanet/releases/download/0.5.1/resnet50_oid_v1.0.0.h5
https://github.com/fizyr/keras-retinanet/releases/download/0.5.1/resnet50_oid_v1.0.0.h5
https://github.com/fizyr/keras-retinanet/releases/download/0.5.1/resnet152_oid_v1.0.0.h5
## 数据集下载 ##

## 数据集制作 ##
- CSV数据集
在目录下建立CSV目录，目录下再建立phtot, xml两个目录
结构为：
>CSV
>>photo
>>xml
>>CSVdata.py

CSVdata.py为制作CSV格式的数据集
```
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
* 说明：制作CSV格式
* 时间：2019-6-27

* 作者： whg
'''
import  xml.dom.minidom
import os,shutil,glob, csv
'''--------------------------配置文件------------------------'''
# XML文件的目录
xmlpath = 'xml'
# 图片的目录
photopath = 'photo'
'''--------------------------主体程序------------------------'''
xmlpath = os.path.join(os.getcwd(), xmlpath)
photopath = os.path.join(os.getcwd(), photopath)
def getxmlinfo(filename):
    dom = xml.dom.minidom.parse(filename)
    root = dom.documentElement
    namelist = root.getElementsByTagName('filename')
    name = namelist[0]
    label = list()
    objectlist = root.getElementsByTagName('object')
    for object in objectlist:
        flaglist = object.getElementsByTagName('name')
        flag = flaglist[0]
        xminlist = object.getElementsByTagName('xmin')
        xmin = xminlist[0]
        yminlist = object.getElementsByTagName('ymin')
        ymin = yminlist[0]
        xmaxlist = object.getElementsByTagName('xmax')
        xmax = xmaxlist[0]
        ymaxlist = object.getElementsByTagName('ymax')
        ymax = ymaxlist[0]
        label += list([flag.firstChild.data, xmin.firstChild.data, ymin.firstChild.data, xmax.firstChild.data, ymax.firstChild.data])
    return name.firstChild.data, label

class CSVDate():
    def __init__(self, photopath = 'photo', xmlpath = 'xml'):
        self.csv = []
        self.photopath = photopath
        self.xmlpath = xmlpath
        self.test_size = 0.1
        self.label = []

        self.getcsv()
        print(self.csv)
        self.write_file()

    def getcsv(self):
        xmllist = os.listdir(self.xmlpath)
        for xml in xmllist:
            _, date = getxmlinfo(os.path.join(self.xmlpath, xml))
            name = xml.split(".xml")[0]
            for i in range(int(len(date)/5)):
                classname = date[i * 5 + 0]
                if classname[0] == '0' or classname[0] == '1':
                    classname = classname[1:]
                if classname not in self.label:
                    self.label.append(classname)
                self.csv.append([os.path.join('photo', name+ '.jpg'), date[i*5 + 1], date[i*5 + 2], date[i*5 + 3], date[i*5 + 4],classname])

    def write_file(self):
        with open('train_annotations.csv', 'w', newline='') as fp:
            csv_writer = csv.writer(fp, dialect='excel')
            csv_writer.writerows(self.csv[int(len(self.csv)*self.test_size):])
            csv_writer.writerows(self.csv[:5])
        with open('val_annotations.csv', 'w', newline='') as fp:
            csv_writer = csv.writer(fp, dialect='excel')
            csv_writer.writerows(self.csv[:int(len(self.csv)*self.test_size)])

        class_name = sorted(self.label)
        class_ = []
        for num, name in enumerate(class_name):
            class_.append([name, num])
        with open('classes.csv', 'w', newline='') as fp:
            csv_writer = csv.writer(fp, dialect='excel')
            csv_writer.writerows(class_)


xml_file = glob.glob('./CSV/xml/*.xml')
CSVDate(photopath, xmlpath)

```
运行命令
```
cd CSV
python CSVdata.py
cd ..
```

## 模型训练 ## 
```
python keras_retinanet/bin/train.py --steps=10 --epochs=1 --weights=snapshots/resnet50_coco_best_v2.1.0.h5 csv CSV/train_annotations.csv CSV/classes.csv --val-annotations CSV/val_annotations.csv
```
## 模型转换 ##
```
python keras_retinanet/bin/convert_model.py snapshots/resnet50_csv_01.h5 snapshots/resnet50_w_713_1.h5
```
## 模型验证 ##
```

```
## 模型测试 ## 
简单例程
```
import keras

# import keras_retinanet
from keras_retinanet import models
from keras_retinanet.utils.image import read_image_bgr, preprocess_image, resize_image
from keras_retinanet.utils.visualization import draw_box, draw_caption
from keras_retinanet.utils.colors import label_color

# import miscellaneous modules
import matplotlib.pyplot as plt
import cv2
import os
import numpy as np
import time
import sys

# set tf backend to allow memory to grow, instead of claiming everything
import tensorflow as tf

def get_session():
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    return tf.Session(config=config)

# use this environment flag to change which GPU to use
#os.environ["CUDA_VISIBLE_DEVICES"] = "1"

# set the modified tf session as backend in keras
keras.backend.tensorflow_backend.set_session(get_session())

# adjust this to point to your downloaded/trained model
# models can be downloaded here: https://github.com/fizyr/keras-retinanet/releases
model_path = os.path.join('snapshots', 'resnet50_coco_best_v2.1.0.h5')

# load retinanet model
model = models.load_model(model_path, backbone_name='resnet50')

# if the model is not converted to an inference model, use the line below
# see: https://github.com/fizyr/keras-retinanet#converting-a-training-model-to-inference-model
#model = models.convert_model(model)

print(model.summary())

# load label to names mapping for visualization purposes
labels_to_names = {0: 'person', 1: 'bicycle', 2: 'car', 3: 'motorcycle', 4: 'airplane', 5: 'bus', 6: 'train', 7: 'truck', 8: 'boat', 9: 'traffic light', 10: 'fire hydrant', 11: 'stop sign', 12: 'parking meter', 13: 'bench', 14: 'bird', 15: 'cat', 16: 'dog', 17: 'horse', 18: 'sheep', 19: 'cow', 20: 'elephant', 21: 'bear', 22: 'zebra', 23: 'giraffe', 24: 'backpack', 25: 'umbrella', 26: 'handbag', 27: 'tie', 28: 'suitcase', 29: 'frisbee', 30: 'skis', 31: 'snowboard', 32: 'sports ball', 33: 'kite', 34: 'baseball bat', 35: 'baseball glove', 36: 'skateboard', 37: 'surfboard', 38: 'tennis racket', 39: 'bottle', 40: 'wine glass', 41: 'cup', 42: 'fork', 43: 'knife', 44: 'spoon', 45: 'bowl', 46: 'banana', 47: 'apple', 48: 'sandwich', 49: 'orange', 50: 'broccoli', 51: 'carrot', 52: 'hot dog', 53: 'pizza', 54: 'donut', 55: 'cake', 56: 'chair', 57: 'couch', 58: 'potted plant', 59: 'bed', 60: 'dining table', 61: 'toilet', 62: 'tv', 63: 'laptop', 64: 'mouse', 65: 'remote', 66: 'keyboard', 67: 'cell phone', 68: 'microwave', 69: 'oven', 70: 'toaster', 71: 'sink', 72: 'refrigerator', 73: 'book', 74: 'clock', 75: 'vase', 76: 'scissors', 77: 'teddy bear', 78: 'hair drier', 79: 'toothbrush'}


photopath = 'examples\\000000008021.jpg'
image = read_image_bgr(photopath)

# copy to draw on
draw = image.copy()
draw = cv2.cvtColor(draw, cv2.COLOR_BGR2RGB)

# preprocess image for network
image = preprocess_image(image)
image, scale = resize_image(image)

# process image
start = time.time()
boxes, scores, labels = model.predict_on_batch(np.expand_dims(image, axis=0))
print("processing time: ", time.time() - start)

# correct for image scale
boxes /= scale

# visualize detections
for box, score, label in zip(boxes[0], scores[0], labels[0]):
   # scores are sorted so we can break
   if score < 0.05:
      continue
   print('label', label)
   color = label_color(label)

   b = box.astype(int)
   draw_box(draw, b, color=color)

   caption = "{} {:.3f}".format(labels_to_names[label], score)
   draw_caption(draw, b, caption)
   print('testphoto' + ' : ' + caption + ' (' , b[0], ',', b[1], ',', b[2], ',', b[3], ')')

plt.figure(figsize=(15, 15))
plt.axis('off')
plt.imshow(draw)
# plt.savefig(f"output\\{photoname}", bbox_inches="tight", pad_inches=0.0)
   #plt.show()
```

---
**签名：Smile every day**    
**名字：沐阳宏**   
**邮箱：whghcyx@outlook.com**  
---
